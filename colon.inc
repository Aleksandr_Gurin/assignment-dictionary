%define next 0

%macro colon 2
  %ifstr %1
    %2: dq next
    db %1, 0
  %else
    %error "The first colon arg is not a string"
  %endif
  %ifid %2
    %define next %2
  %else
    %error "The second colon arg is an invalid label"
  %endif
%endmacro