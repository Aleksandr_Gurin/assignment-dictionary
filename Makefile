ASM=nasm
ASMFLAGS=-f elf64
LD=ld

all: main

%.o: %.asm
	$(ASM) $(ASMFLAGS) -o $@ $<

main: main.o dict.o lib.o
	$(LD) -o $@ $^

clean:
	rm -rf *.o main

.PHONY: clean all
