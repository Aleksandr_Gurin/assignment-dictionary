section .text

global exit
global string_length
global print_string
global print_char
global print_newline
global print_uint
global print_int
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy
global print_error

; Принимает код возврата и завершает текущий процесс
exit:
    mov rax, 60
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
    .loop:
        cmp byte [rdi+rax], 0
        je .end
        inc rax
        jmp .loop
    .end:
        ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi
    call string_length
    pop rdi
    mov     rdx, rax
    mov     rsi, rdi
    mov     rax, 1
    mov     rdi, 1
    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rdi, 1
    mov rdx, 1
    mov rax, 1
    mov rsi, rsp
    syscall
    pop rdi
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, 0xA
    jmp print_char

; Выводит беззнаковое 8-байтовое число в десятичном формате
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
	mov rax,rdi
	mov r10, 10
	mov r8, rsp  ;address return
	dec rsp
	mov byte[rsp], 0
.loop:
	xor rdx, rdx
	div r10
	add rdx, '0' ;to ASCII
	dec rsp
	mov byte [rsp], dl
	test rax, rax;
	jne .loop
	mov rdi, rsp
	call print_string
	mov rsp, r8
	ret

; Выводит знаковое 8-байтовое число в десятичном формате
print_int:
    test rdi, rdi
	jns .plus
	neg rdi
	push rdi
	mov rdi, '-'
	call print_char
	pop rdi
.plus:
	call print_uint
	ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    mov al, byte [rdi]
    cmp al, byte [rsi]
    jne .not_equal
    inc rdi
    inc rsi
    test al, al
    jnz string_equals
    mov rax, 1
    ret
    .not_equal:
        mov rax, 0
        ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    push 0
    xor rax, rax
    xor rdi, rdi
    mov rsi, rsp
    mov rdx, 1
    syscall
    pop rax
    ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
read_word:
	mov r8, rdi ; buff start
	mov r9, rsi ;size
	xor r10, r10
.space:
	call read_char
	cmp rax, 0x20
	je .space
	cmp rax, 0x9
	je .space
	cmp rax, 0xA
	je .space
	je .end
	test rax, rax

.loop:
	cmp r9, r10
	je .fail
	test rax, rax
	jz .end

	mov [r8+r10], rax
	inc r10
	call read_char
	cmp rax, 0x20
	je .end
	cmp rax, 0x9
	je .end
	cmp rax, 0xA
	je .end

	jmp .loop

.fail:
	xor rax, rax
	ret
.end:
	mov rax, r8
	mov rdx, r10
	mov byte[r8+r10],0
	ret


; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    mov rax, 0
    mov r9, 0
    mov r10, 10
    mov rdx, 0
    .read_num:
    	mov r9b, [rdi + rdx]
    	cmp r9b, '9'
    	ja .fault
    	cmp r9b, '0'
    	jb .fault
    	sub r9b, '0'
    	push rdx
    	mul r10
    	pop rdx
    	add rax, r9
    	inc rdx
    	jmp .read_num
    .fault:
    	ret




; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был)
; rdx = 0 если число прочитать не удалось
parse_int:
    cmp byte [rdi], '-'
        jne .success
        inc rdi
        call parse_uint
        cmp rdx, 0
        je .fault
        inc rdx
        neg rax
        ret
    .fault:
        xor rax,rax
        ret
    .success:
        jmp parse_uint


; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor r10, r10
    xor rax, rax
    push rdi
    mov rdi, rsi
    pop rsi
    call string_length
    mov rcx, rax
    rep movsb
    mov rax, r10
    xor r10, r10
    ret

print_error:
	call string_length
	mov rdx, rax
	mov rax, 1
	mov rsi, rdi
	mov rdi, 2
	syscall
	ret