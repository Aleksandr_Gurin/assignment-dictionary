%include "words.inc"
%include "lib.inc"
extern find_word

section .data
size_overflow_error: db "String should have less than 255 characters", 10, 0
not_found_error: db "Not found", 10, 0
buf: times 255 db 0

section .text
global _start

_start:
	mov rdi, buf
	mov rsi, 255
	call read_word
	test rax, rax
	jz .size_overflow
	mov rdi, rax
	mov rsi, next
	call find_word
	test rax, rax
	jz .not_found
	mov rdi, rax
	add rdi, 8
	push rdi
	call string_length
	pop rdi
	inc rax
	add rdi,rax
	call print_string
	call print_newline
	call exit
.size_overflow:
	mov rdi, size_overflow_error
	call print_error
	call exit
.not_found:
	mov rdi, not_found_error
	call print_error
	call exit
